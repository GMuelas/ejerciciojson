# Ejercicio Clarive

# Realiza una peticion HTTP al servidor especificado y analiza la respuesta obtenida (en formato JSON).
# A continuacion calcula el factorial del numero obtenido como respuesta.

# Librerias para manejo de peticiones y respuestas HTTP (para Python 2.7)
import urllib2
import json


# Calcula el factorial de un numero 'n'
def calculaFactorial(n):

	if (n == 0):
		return 1;
	
	return n * calculaFactorial(n-1)


# Enviamos una peticion HTTP al servidor y guardamos la respuesta
server_url = 'http://support.clarive.com/rule/json/test?user=generic'
response = urllib2.urlopen(server_url)

print("Se ha enviado la peticion al servidor " + str(server_url))

json_string = response.read()

# Por medio del modulo json leemos el atributo 'number' y lo almacenamos
json_obj = json.loads(json_string)

#Obtenemos el atributo de interes
number = json_obj['number']

print("El valor del atributo 'number' es: " + str(number))

fact = calculaFactorial(int(number))
print("El factorial de " + str(number) + " es: " + str(fact))


